<?php
require_once('IGrossPrice.php');
require_once('ItemPrice.php');

class GrossPrice implements IGrossPrice
{
    private $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function calculatorFee()
    {
        $gross_price = 0;
        foreach ($this->order as $key => $item) {
            $item_price = new ItemPrice($key, $item);
            $gross_price = $gross_price + $item_price->calculatorFee();
        }

        return $gross_price;
    }
}
