<?php
require_once('IGrossPrice.php');
require_once('ShippingFee.php');

require_once('FeeByWeight.php');
require_once('FeeByDimension.php');

require_once('Attribute.php');
require_once('Value.php');
require_once('Entity.php');

class ItemPrice implements IGrossPrice
{
    private $amazon_price;

    private $name;

    private $item;

    public function __construct($name, $item)
    {
        $this->amazon_price = $item['amazon_price'];
        unset($item['amazon_price']);
        $this->item = $item;
        $this->name = $name;
    }

    public function calculatorFee()
    {
        $list_shipping_fee = [];
        foreach ($this->item as $key => $val) {
            $attribute = new Attribute($key);
            switch ($key) {
                case 'weight_coefficient':
                    $fee_by_weight = new FeeByWeight(
                        $this->item['weight'],
                        $this->item['weight_coefficient']
                    );

                    $list_shipping_fee[] = new Value($attribute, $fee_by_weight->FeeByWeight());
                    break;
                case 'dimension_coefficient':
                    $fee_by_dimension = new FeeByDimension(
                        $this->fee_by_dimension['width'],
                        $this->fee_by_dimension['height'],
                        $this->fee_by_dimension['depth'],
                        $this->fee_by_dimension['dimension_coefficient']
                    );

                    $list_shipping_fee[] = new Value($attribute, $fee_by_dimension->FeeByDimension());
                    break;
                default:
                    $list_shipping_fee[] = new Value($attribute, $val);
                    break;
            }
        }

        $entity = new Entity($this->name, $list_shipping_fee);
        $list_shipping_fee = $entity->listShippingFee();
        $shipping_fee = new ShippingFee($list_shipping_fee[$this->name]);
        $item_price = $this->amazon_price + $shipping_fee->totalPrice();

        return $item_price;
    }
}
