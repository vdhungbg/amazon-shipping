<?php

class FeeByWeight
{
    private $product_weight;

    private $weight_coefficient;

    public function __construct($product_weight, $weight_coefficient)
    {
        $this->product_weight = $product_weight;
        $this->weight_coefficient = $weight_coefficient;
    }

    public function FeeByWeight()
    {
        $fee_by_weight = $this->product_weight * $this->weight_coefficient;

        return $fee_by_weight;
    }
}
