<?php
require_once('IGrossPrice.php');

class CalculatorFeeRender
{
    public function process(IGrossPrice $interface_gross_price)
    {
        return $interface_gross_price->calculatorFee();
    }
}
