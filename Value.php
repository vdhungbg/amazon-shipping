<?php
declare(strict_types=1);

namespace DesignPatterns\More\EAV;

class Value
{
    /**
     * @var Attribute
     */
    private $attribute;

    /**
     * @var string
     */
    private $value;

    public function __construct(Attribute $attribute, $value)
    {
        $this->value = $value;
        $this->attribute = $attribute;

        $attribute->addValue($this);
    }

    public function __toString()
    {
        return sprintf('%s: %s', $this->attribute, $this->name);
    }
}
