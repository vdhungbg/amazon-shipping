<?php
declare(strict_types=1);

namespace DesignPatterns\More\EAV;

class Attribute
{
    private $values;

    private $name;

    public function __construct($name)
    {
        $this->values = new \SplObjectStorage();
        $this->name = $name;
    }

    public function addValue(Value $value)
    {
        $this->values->attach($value);
    }

    public function getValues()
    {
        return $this->values;
    }

    public function __toString()
    {
        return $this->name;
    }
}
