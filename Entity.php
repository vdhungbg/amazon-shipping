<?php
declare(strict_types=1);

namespace DesignPatterns\More\EAV;

class Entity
{
    private $values;

    private $name;

    public function __construct($name, $values)
    {
        $this->values = new \SplObjectStorage();
        $this->name = $name;

        foreach ($values as $value) {
            $this->values->attach($value);
        }
    }

    public function listShippingFee()
    {
        $list_shipping_fee = [];
        foreach ($this->values as $value) {
            $list_shipping_fee[] = $value;
        }

        return [$this->name => $list_shipping_fee];
    }
}
