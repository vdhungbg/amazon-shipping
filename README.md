# 3. Exercises
## Exercise for Entity-Attribute-Value pattern
### Our story

There is a shipping service.
It helps Vietnamese buy products on Amazon website.
After a client provides items urls on Amazon, we will have: Amazon price, product weight, width, height, depth

We need to calculate a gross price for an order (contains many items) follow formulas:

gross price = item price 1 + item price 2 + ...

item price = amazon price + shipping fee

shipping fee = max (fee by weight, fee by dimensions, ...)
=> phí vận chuyển = tối đa (phí theo trọng lượng, phí theo kích thước)

fee by weight = product weight x weight coefficient
=> phí theo trọng lượng = trọng lượng sản phẩm x hệ số trọng lượng

fee by dimension = width x height x depth x dimension coefficient
=> phí theo kích thước = chiều rộng x chiều cao x chiều sâu x hệ số kích thước

Example coefficients:
weight coefficient: $11/kg
dimension coefficient: $11/m3

# What do you need to do?
## Create the necessary and flexible structure of classes to calculate the gross price for an order.
=> Tạo cấu trúc cần thiết và linh hoạt của các lớp để tính tổng giá cho một đơn hàng.
## Write unit tests for classes
=> Viết bài kiểm tra đơn vị cho các lớp

# Requirements:

## 1. No need to implement UI
## 2. Don't use any framework

## 3. Code must be runnable without any exceptions
=> Mã phải được chạy mà không có ngoại lệ
## 4. Code must satisfy PSR-2
=> Mã phải đáp ứng PSR-2
## 5. Classes must satisfy S.O.L.I.D principles
## 6. Coefficients are configurable
=> Hệ số có thể định cấu hình
## 7. Use PHP7 if possible

## 8. Shipping fee must be flexible. For example, shiping_fee for a smartphone (300g) must be less than a diamond ring
(10g) so we need be add fee by product type someday
=> Phí vận chuyển phải linh hoạt. Ví dụ: shiping_fee cho điện thoại thông minh (300g) phải nhỏ hơn nhẫn kim cương
(10g) vì vậy chúng tôi cần thêm phí theo loại sản phẩm vào một ngày nào đó

### shipping fee = max (fee by weight, fee by dimensions, fee by product type)
=> phí vận chuyển = tối đa (phí theo trọng lượng, phí theo kích thước, phí theo loại sản phẩm)

### How do we add fee by product type without change shipping fee code?
=> Làm cách nào để thêm phí theo loại sản phẩm mà không thay đổi mã phí giao hàng?

### No need to implement fee by product type
=> Không cần thực hiện phí theo loại sản phẩm

