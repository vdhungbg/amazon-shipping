<?php
require_once('IGrossPrice.php');

class ShippingFee implements IGrossPrice
{
    private $list_shipping_fee;

    public function __construct($list_shipping_fee)
    {
        $this->list_shipping_fee = $list_shipping_fee;
    }

    public function calculatorFee()
    {
        return max($this->list_shipping_fee);
    }
}
