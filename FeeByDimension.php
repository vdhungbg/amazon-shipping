<?php

class FeeByDimension
{
    private $width;

    private $height;

    private $depth;

    private $dimension_coefficient;

    public function __construct($width, $height, $depth, $dimension_coefficient)
    {
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;
        $this->dimension_coefficient = $dimension_coefficient;
    }

    public function FeeByDimension()
    {
        $fee_by_dimension = $this->width * $this->height * $this->depth * $this->dimension_coefficient;

        return $fee_by_dimension;
    }
}
