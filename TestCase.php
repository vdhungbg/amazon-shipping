<?php
require_once('CalculatorFeeRender.php');
require_once('GrossPrice.php');

class TestCase
{
    public function testGrossPriceAllItems()
    {
        $order = [
            'item-1' => [
                'amazon_price' => 10,
                'weight' => 20,
                'weight_coefficient' => 2,
            ],
            'item-2' => [
                'amazon_price' => 15,
                'width' => 20,
                'height' => 20,
                'depth' => 20,
                'dimension_coefficient' => 1,
            ],
            'item-3' => [
                'amazon_price' => 20,
                'fee' => 40,
            ],
            'item-4' => [
                'amazon_price' => 20,
                'weight' => 30,
                'weight_coefficient' => 2,
                'width' => 30,
                'height' => 30,
                'depth' => 30,
                'dimension_coefficient' => 1,
            ],
            'item-5' => [
                'amazon_price' => 20,
                'fee' => 50,
                'weight' => 30,
                'weight_coefficient' => 2,
                'width' => 30,
                'height' => 30,
                'depth' => 30,
                'dimension_coefficient' => 1,
            ]
        ];

        $gross_price = new GrossPrice($order);
        $calculator_fee = new CalculatorFeeRender();

        return $calculator_fee->process($gross_price);
    }
}
